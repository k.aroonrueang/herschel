#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 17:49:08 2023

@author: mamodnoii
"""

import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
import fireworks.nbodylib.integrators as fint
import matplotlib.pyplot as plt
plt.style.use('default')
import os 

'''plz check all updates of Fireworks on https://gitlab.com/k.aroonrueang/herschel'''

###############################################################################
############ ------------ PRELIMINARY  ------------ ############

### --- Create directories and subdirectories to save all results
parent_dir = './plot_AS3D_ecc0-99/'
sub_dir_arr =['AS3D_Eerror','AS3D_Etot','AS3D_xy_Euler','AS3D_xy_LF','AS3D_xy_RK4','AS3D_xy_Tsunami']
if not os.path.exists(parent_dir): 
    for sub_dir in sub_dir_arr:
        os.makedirs(parent_dir+sub_dir)


### --- Initial Condition 
# -- Nbodyunit-values
#Mscale=10Msun
#m1_physical=10Msun
#m2_physical=20Msun
#Lscale=20AU
#rp_physical=2AU
m1_Nbody=1.
m2_Nbody=2.
rp_Nbody=0.1
e=0.99
particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)

#Calculate Tperiod
a_Nbody=-(m1_Nbody*m2_Nbody)/(2.*particles.Etot(softening= 0.0)[0])
#a_Nbody=rp_Nbody/(1.-e)
Tperiod = 2.*np.pi*np.sqrt(a_Nbody**3/(m1_Nbody+m2_Nbody))
h_arr = [1e-1,1e-2,1e-3] #constant time steps
tf=Tperiod*10.

### --- name of plots
xy_Euler_path = './plot_AS3D_ecc0-99/AS3D_xy_Euler/AS3D_xy_Euler_'
xy_Euler_plot = [xy_Euler_path+'h'+'{:.0e}'.format(i)+'_ecc0-99.png' for i in h_arr]

xy_LF_path = './plot_AS3D_ecc0-99/AS3D_xy_LF/AS3D_xy_LF_'
xy_LF_plot = [xy_LF_path+'h'+'{:.0e}'.format(i)+'_ecc0-99.png' for i in h_arr]

xy_RK4_path = './plot_AS3D_ecc0-99/AS3D_xy_RK4/AS3D_xy_RK4_'
xy_RK4_plot = [xy_RK4_path+'h'+'{:.0e}'.format(i)+'_ecc0-99.png' for i in h_arr]

xy_Tsunami_plot = './plot_AS3D_ecc0-99/AS3D_xy_Tsunami/AS3D_xy_Tsunami_ecc0-99.png'

Etot_path = './plot_AS3D_ecc0-99/AS3D_Etot/AS3D_Etot_'
Etot_plot = [Etot_path+'h'+'{:.0e}'.format(i)+'_ecc0-99.png' for i in h_arr]
Eerror_plot = './plot_AS3D_ecc0-99/AS3D_Eerror/AS3D_Eerror-avg_ecc0-99.png'
Eerror_path = './plot_AS3D_ecc0-99/AS3D_Eerror/'
Eerror_each_plot = [Eerror_path+'AS3D_Eerror_Euler_ecc0-99.png',Eerror_path+'AS3D_Eerror_LF_ecc0-99.png',\
                    Eerror_path+'AS3D_Eerror_RK4_ecc0-99.png',Eerror_path+'AS3D_Eerror_Tsunami_ecc0-99.png']


### ---  keys of dicts
#tarr
key_tarr = ['t_arr_'+'{:.0e}'.format(i) for i in h_arr]
#Etot for each integrator
key_Etot_Euler = ['Euler_Etot_'+'{:.0e}'.format(i) for i in h_arr]
key_Etot_LF= ['LF_Etot_'+'{:.0e}'.format(i) for i in h_arr]
key_Etot_RK4 = ['RK4_Etot_'+'{:.0e}'.format(i) for i in h_arr]
#Eerror for each integrator
key_Eerror_Euler_each = ['Eerror_Euler_'+'{:.0e}'.format(i) for i in h_arr]
key_Eerror_LF_each = ['Eerror_LF_'+'{:.0e}'.format(i) for i in h_arr]
key_Eerror_RK4_each = ['Eerror_RK4_'+'{:.0e}'.format(i) for i in h_arr]


### --- initialize dicts
#tarr
tarr_dict = {}
#Etot for each integrator
Etot_Euler_dict = {}
Etot_LF_dict = {}
Etot_RK4_dict = {}
#Eerror for each integrator
Eerror_Euler_each_dict = {}
Eerror_LF_each_dict = {}
Eerror_RK4_each_dict = {}


###############################################################################
############ ------------ CALCULATION + XYPLOT  ------------ ############
### -----  fint.integrator_euler ------
for idx,h in enumerate(h_arr):

    particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)
    
    t=0.
    t_arr=[]
    x1_arr=[]
    y1_arr=[]
    x2_arr=[]
    y2_arr=[]
    Etot_Euler_arr=[]
    while(t<tf):
        t_arr.append(t)
        t+=h
                                             
        particles, tstep, acc, jerk, potential=fint.integrator_euler(particles=particles,
                            tstep=h,
                            acceleration_estimator=fdyn.acceleration_direct_vectorised,
                            softening=0.,
                            external_accelerations = None)
        x1_arr.append(particles.pos[0][0]) #m1 x
        y1_arr.append(particles.pos[0][1]) #m1 y
        x2_arr.append(particles.pos[1][0]) #m2 x
        y2_arr.append(particles.pos[1][1]) #m2 y
        Etot_Euler_arr.append(particles.Etot(softening= 0.0)[0])
      
    x1_arr=np.array(x1_arr)
    y1_arr=np.array(y1_arr)
    x2_arr=np.array(x2_arr)
    y2_arr=np.array(y2_arr)
    t_arr=np.array(t_arr)
    Etot_Euler_arr=np.array(Etot_Euler_arr)

    tarr_dict[key_tarr[idx]] = t_arr
    Etot_Euler_dict[key_Etot_Euler[idx]] = Etot_Euler_arr
 
    
    ### PLOT XY EULER
    fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
    fig.patch.set_facecolor('white')
    plt.plot(x1_arr, y1_arr, color='r',linestyle="-",label="m1_Nbody=1")
    plt.plot(x2_arr, y2_arr, color='b',linestyle="-",label="m2_Nbody=2")
    plt.legend(fontsize='10', loc='upper right')
    plt.xlabel('x [nbody]', fontsize=13)
    plt.ylabel('y [nbody]', fontsize=13)
    plt.title('Euler, h='+str(h)+', e=0.99', fontsize=13)
    plt.tight_layout()
    plt.savefig(xy_Euler_plot[idx], format="png",dpi=300)
    plt.show()


### -----  fint.integrator_leapfrog ------
for idx,h in enumerate(h_arr):

    particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)
    
    t=0.
    t_arr=[]
    x1_arr=[]
    y1_arr=[]
    x2_arr=[]
    y2_arr=[]
    Etot_LF_arr=[]
    while(t<tf):
        t_arr.append(t)
        t+=h
                                             
        particles, tstep, acc, jerk, potential=fint.integrator_leapfrog(particles=particles,
                            tstep=h,
                            acceleration_estimator=fdyn.acceleration_direct_vectorised,
                            softening=0.,
                            external_accelerations = None)
        x1_arr.append(particles.pos[0][0]) #m1 x
        y1_arr.append(particles.pos[0][1]) #m1 y
        x2_arr.append(particles.pos[1][0]) #m2 x
        y2_arr.append(particles.pos[1][1]) #m2 y
        Etot_LF_arr.append(particles.Etot(softening= 0.0)[0])
      
    x1_arr=np.array(x1_arr)
    y1_arr=np.array(y1_arr)
    x2_arr=np.array(x2_arr)
    y2_arr=np.array(y2_arr)
    t_arr=np.array(t_arr)
    Etot_LF_arr=np.array(Etot_LF_arr)
    Etot_LF_dict[key_Etot_LF[idx]]= Etot_LF_arr
    
    ### PLOT XY LF
    fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
    fig.patch.set_facecolor('white')
    plt.plot(x1_arr, y1_arr, color='r',linestyle="-",label="m1_Nbody=1")
    plt.plot(x2_arr, y2_arr, color='b',linestyle="-",label="m2_Nbody=2")
    plt.legend(fontsize='10', loc='upper right')
    plt.xlabel('x [nbody]', fontsize=13)
    plt.ylabel('y [nbody]', fontsize=13)
    plt.title('Leapfrog, h='+str(h)+', e=0.99', fontsize=13)
    plt.tight_layout()
    plt.savefig(xy_LF_plot[idx], format="png",dpi=300)
    plt.show()


### ----- fint.integrator_rk4 ------
for idx,h in enumerate(h_arr):
    particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)
    
    t=0.
    t_arr=[]
    x1_arr=[]
    y1_arr=[]
    x2_arr=[]
    y2_arr=[]
    Etot_RK4_arr=[]
    while(t<tf):
        t_arr.append(t)
        t+=h
                                             
        particles, tstep, acc, jerk, potential=fint.integrator_rk4(particles=particles,
                            tstep=h,
                            acceleration_estimator=fdyn.acceleration_direct_vectorised,
                            softening=0.,
                            external_accelerations = None)
        x1_arr.append(particles.pos[0][0]) #m1 x
        y1_arr.append(particles.pos[0][1]) #m1 y
        x2_arr.append(particles.pos[1][0]) #m2 x
        y2_arr.append(particles.pos[1][1]) #m2 y
        Etot_RK4_arr.append(particles.Etot(softening= 0.0)[0])
      
    x1_arr=np.array(x1_arr)
    y1_arr=np.array(y1_arr)
    x2_arr=np.array(x2_arr)
    y2_arr=np.array(y2_arr)
    t_arr=np.array(t_arr)
    Etot_RK4_arr=np.array(Etot_RK4_arr)
    Etot_RK4_dict[key_Etot_RK4[idx]]= Etot_RK4_arr
    
    ### PLOT XY RK4
    fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
    fig.patch.set_facecolor('white')
    plt.plot(x1_arr, y1_arr, color='r',linestyle="-",label="m1_Nbody=1")
    plt.plot(x2_arr, y2_arr, color='b',linestyle="-",label="m2_Nbody=2")
    plt.legend(fontsize='10', loc='upper right')
    plt.xlabel('x [nbody]', fontsize=13)
    plt.ylabel('y [nbody]', fontsize=13)
    plt.title('RK4, h='+str(h)+', e=0.99', fontsize=13)  
    plt.tight_layout()
    plt.savefig(xy_RK4_plot[idx], format="png",dpi=300)
    plt.show()

### -----  fint.integrator_tsunami ------
tcurrent = 0
x1_arr=[]
y1_arr=[]
x2_arr=[]
y2_arr=[]
step=0.01
t_Tsunami_arr=np.arange(tcurrent, tf+step, step)
Etot_Tsunami_arr=[]
efftime_arr=[]
tplot_Tsunami_arr=[]
for t in t_Tsunami_arr:
    tstep=t-tcurrent
    # skip this loop for tstep <= 0 (negative and zero tstep will raise error: you try to evolve the system that was already evolved)
    if tstep <= 0: continue 
    tplot_Tsunami_arr.append(tcurrent)
    particles, efftime,_, _,_=fint.integrator_tsunami(particles,tstep)
    tcurrent=tcurrent+efftime
    
    x1_arr.append(particles.pos[0][0]) #m1 x
    y1_arr.append(particles.pos[0][1]) #m1 y
    x2_arr.append(particles.pos[1][0]) #m2 x
    y2_arr.append(particles.pos[1][1]) #m2 y
    Etot_Tsunami_arr.append(particles.Etot(softening= 0.0)[0])
    efftime_arr.append(efftime)
  
x1_arr=np.array(x1_arr)
y1_arr=np.array(y1_arr)
x2_arr=np.array(x2_arr)
y2_arr=np.array(y2_arr)
Etot_Tsunami_arr=np.array(Etot_Tsunami_arr)
tplot_Tsunami_arr=np.array(tplot_Tsunami_arr)

### PLOT XY Tsunami
fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.scatter(x1_arr, y1_arr, marker='.', s=7,color='r',linestyle="None",label="m1_Nbody=1")
plt.scatter(x2_arr, y2_arr, marker='.', s=7, color='b',linestyle="None",label="m2_Nbody=2")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('x [nbody]', fontsize=13)
plt.ylabel('y [nbody]', fontsize=13)
plt.title('Tsunami - Adaptive timestep, e=0.99', fontsize=13)  
plt.tight_layout()
plt.savefig(xy_Tsunami_plot, format="png",dpi=300)
plt.show()


###############################################################################
############ ------------ ETOT_VS_TIME-PLOT for all ------------ ############
for idx,h in enumerate(h_arr):
    fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
    fig.patch.set_facecolor('white')
    plt.plot(tarr_dict[key_tarr[idx]],Etot_Euler_dict[key_Etot_Euler[idx]], color='C1',linestyle="-",label="Euler")
    plt.plot(tarr_dict[key_tarr[idx]],Etot_LF_dict[key_Etot_LF[idx]], color='C2',linestyle="-",label="LF")
    plt.plot(tarr_dict[key_tarr[idx]],Etot_RK4_dict[key_Etot_RK4[idx]], color='C3',linestyle="-",label="RK4")
    plt.plot(tplot_Tsunami_arr,Etot_Tsunami_arr, color='C4',linestyle="-",label="Tsunami")
    plt.legend(fontsize='10', loc='upper right')
    plt.xlabel('t [nbody]', fontsize=13)
    plt.ylabel('Etot [nbody]', fontsize=13)
    plt.title('h='+str(h)+', e=0.99', fontsize=13)
    #plt.yscale("log")
    plt.tight_layout()
    plt.savefig(Etot_plot[idx], format="png",dpi=300)
    plt.show()

###############################################################################
############ ------------ Eerror_VS_TSTEP-PLOT ------------ ############
### --- name of dicts' keys of Eerror
key_Eerror_Euler = ['Euler_Eerror_'+'{:.0e}'.format(i) for i in h_arr]
key_Eerror_LF= ['LF_Eerror_'+'{:.0e}'.format(i) for i in h_arr]
key_Eerror_RK4 = ['RK4_Eerror_'+'{:.0e}'.format(i) for i in h_arr]
key_Eerror_Tsunami = 'Tsunami_Eerror'

### --- initialize dicts of Eerror
Eerror_Euler_dict = {}
Eerror_LF_dict = {}
Eerror_RK4_dict = {}
Eerror_Tsunami_dict = {}

### --- compute and store Eerror of Euler
Eerror_Euler_final=[]
for idx,h in enumerate(h_arr):
    E0=Etot_Euler_dict[key_Etot_Euler[idx]][0]
    Eerror_arr = [np.abs((E-E0)/E0) for E in Etot_Euler_dict[key_Etot_Euler[idx]]]
    Eerror_avg = np.average(Eerror_arr)
    Eerror_Euler_dict[key_Eerror_Euler[idx]]= Eerror_avg
    Eerror_Euler_each_dict[key_Eerror_Euler_each[idx]] = Eerror_arr
    Ef=Etot_Euler_dict[key_Etot_Euler[idx]][-1]
    Eerror_Euler_final.append(np.abs((Ef-E0)/E0))

### --- compute and store Eerror of LF
Eerror_LF_final=[]
for idx,h in enumerate(h_arr):
    E0=Etot_LF_dict[key_Etot_LF[idx]][0]
    Eerror_arr = [np.abs((E-E0)/E0) for E in Etot_LF_dict[key_Etot_LF[idx]]]
    Eerror_avg = np.average(Eerror_arr)
    Eerror_LF_dict[key_Eerror_LF[idx]]= Eerror_avg
    Eerror_LF_each_dict[key_Eerror_LF_each[idx]] = Eerror_arr
    Ef=Etot_LF_dict[key_Etot_LF[idx]][-1]
    Eerror_LF_final.append(np.abs((Ef-E0)/E0))
    
### --- compute and store Eerror of RK4
Eerror_RK4_final=[]
for idx,h in enumerate(h_arr):
    E0=Etot_RK4_dict[key_Etot_RK4[idx]][0]
    Eerror_arr = [np.abs((E-E0)/E0) for E in Etot_RK4_dict[key_Etot_RK4[idx]]]
    Eerror_avg = np.average(Eerror_arr)
    Eerror_RK4_dict[key_Eerror_RK4[idx]]= Eerror_avg
    Eerror_RK4_each_dict[key_Eerror_RK4_each[idx]] = Eerror_arr
    Ef=Etot_RK4_dict[key_Etot_RK4[idx]][-1]
    Eerror_RK4_final.append(np.abs((Ef-E0)/E0))
    
### --- compute and store Eerror of Tsunami
E0=Etot_Tsunami_arr[0]
Eerror_Tsunami_each_arr = [np.abs((E-E0)/E0) for E in Etot_Tsunami_arr]
Eerror_Tsunami_arr = np.average(Eerror_arr)

### --- PLOT avg Eerror

h_arr=np.array(h_arr)
fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(h_arr,Eerror_Euler_dict.values(), color='C1', marker='.',ms=10,linestyle="-",label="Euler")
plt.plot(h_arr,Eerror_LF_dict.values(), color='C2', marker='.',ms=10,linestyle="-",label="LF")
plt.plot(h_arr,Eerror_RK4_dict.values(), color='C3', marker='.',ms=10,linestyle="-",label="RK4")
plt.plot(efftime_arr,Eerror_Tsunami_each_arr , color='C4', marker='.',ms=10,linestyle="-",label="Tsunami")
plt.plot(h_arr,h_arr, color='C1',linestyle="--",alpha=0.5,label='[O(h)]')
plt.plot(h_arr,h_arr**2, color='C2',linestyle="--",alpha=0.5,label='[O(h^2)]')
plt.plot(h_arr,h_arr**4, color='C3',linestyle="--",alpha=0.5,label='[O(h^4)]')
plt.legend(fontsize='10', loc='lower right')
plt.xlabel('h [nbody]', fontsize=13)
plt.ylabel('avg of |E0-E/E0| [nbody]', fontsize=13)
plt.xscale("log")
plt.yscale("log")
plt.title('e=0.99', fontsize=13)
plt.xticks(h_arr, fontsize=10)
plt.tight_layout()
plt.savefig(Eerror_plot, format="png",dpi=300)
plt.show()


### --- PLOT Eerror of final E

h_arr=np.array(h_arr)
fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(h_arr,Eerror_Euler_final, color='C1', marker='.',ms=10,linestyle="-",label="Euler")
plt.plot(h_arr,Eerror_LF_final, color='C2', marker='.',ms=10,linestyle="-",label="LF")
plt.plot(h_arr,Eerror_RK4_final, color='C3', marker='.',ms=10,linestyle="-",label="RK4")
plt.plot(efftime_arr,Eerror_Tsunami_each_arr , color='C4', marker='.',ms=10,linestyle="-",label="Tsunami")
plt.plot(h_arr,h_arr, color='C1',linestyle="--",alpha=0.5,label='[O(h)]')
plt.plot(h_arr,h_arr**2, color='C2',linestyle="--",alpha=0.5,label='[O(h^2)]')
plt.plot(h_arr,h_arr**4, color='C3',linestyle="--",alpha=0.5,label='[O(h^4)]')
plt.legend(fontsize='10', loc='lower right')
plt.xlabel('h [nbody]', fontsize=13)
plt.ylabel('|Ef-E0/E0| [nbody]', fontsize=13)
plt.xscale("log")
plt.yscale("log")
plt.title('e=0.99', fontsize=13)
plt.xticks(h_arr, fontsize=10)
plt.tight_layout()
plt.savefig('./plot_AS3D_ecc0-99/AS3D_Eerror/AS3D_Eerror-final_ecc0-99.png', format="png",dpi=300)
plt.show()


###############################################################################
############ ------------ Eerror_VS_TIME-PLOT ------------ ############

integrator_list = ['Euler','LF','RK4','Tsunami']

### ------- Euler
fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(tarr_dict[key_tarr[0]],Eerror_Euler_each_dict[key_Eerror_Euler_each[0]], color='C1',linestyle="-",label="h=0.1")
plt.plot(tarr_dict[key_tarr[1]],Eerror_Euler_each_dict[key_Eerror_Euler_each[1]], color='C2', linestyle="-",label="h=0.01")
plt.plot(tarr_dict[key_tarr[2]],Eerror_Euler_each_dict[key_Eerror_Euler_each[2]], color='C3', linestyle="-",label="h=0.001")
#plt.plot(tarr_dict[key_tarr[3]],Eerror_Euler_each_dict[key_Eerror_Euler_each[3]], color='C4', linestyle="-",label="h=0.0001")
plt.legend(fontsize='10', loc='lower right')
plt.xlabel('t [nbody]', fontsize=13)
plt.ylabel('|E0-E/E0| [nbody]', fontsize=13)
plt.title(integrator_list[0]+', e=0.99', fontsize=13)
plt.yscale("log")
plt.tight_layout()
plt.savefig(Eerror_each_plot[0], format="png",dpi=300)
plt.show()

### ------- LF
fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(tarr_dict[key_tarr[0]],Eerror_LF_each_dict[key_Eerror_LF_each[0]], color='C1',linestyle="-",label="h=0.1")
plt.plot(tarr_dict[key_tarr[1]],Eerror_LF_each_dict[key_Eerror_LF_each[1]], color='C2', linestyle="-",label="h=0.01")
plt.plot(tarr_dict[key_tarr[2]],Eerror_LF_each_dict[key_Eerror_LF_each[2]], color='C3', linestyle="-",label="h=0.001")
#plt.plot(tarr_dict[key_tarr[3]],Eerror_LF_each_dict[key_Eerror_LF_each[3]], color='C4', linestyle="-",label="h=0.0001")
plt.legend(fontsize='10', loc='lower right')
plt.xlabel('t [nbody]', fontsize=13)
plt.ylabel('|E0-E/E0| [nbody]', fontsize=13)
plt.title(integrator_list[1]+', e=0.99', fontsize=13)
plt.yscale("log")
plt.tight_layout()
plt.savefig(Eerror_each_plot[1], format="png",dpi=300)
plt.show()

### ------- RK4
fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(tarr_dict[key_tarr[0]],Eerror_RK4_each_dict[key_Eerror_RK4_each[0]], color='C1',linestyle="-",label="h=0.1")
plt.plot(tarr_dict[key_tarr[1]],Eerror_RK4_each_dict[key_Eerror_RK4_each[1]], color='C2', linestyle="-",label="h=0.01")
plt.plot(tarr_dict[key_tarr[2]],Eerror_RK4_each_dict[key_Eerror_RK4_each[2]], color='C3', linestyle="-",label="h=0.001")
#plt.plot(tarr_dict[key_tarr[3]],Eerror_RK4_each_dict[key_Eerror_RK4_each[3]], color='C4', linestyle="-",label="h=0.0001")
plt.legend(fontsize='10', loc='lower right')
plt.xlabel('t [nbody]', fontsize=13)
plt.ylabel('|E0-E/E0| [nbody]', fontsize=13)
plt.title(integrator_list[2]+', e=0.99', fontsize=13)
plt.yscale("log")
plt.tight_layout()
plt.savefig(Eerror_each_plot[2], format="png",dpi=300)
plt.show()

### ------- Tsunami
fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(tplot_Tsunami_arr,Eerror_Tsunami_each_arr, color='C1',linestyle="-")
plt.xlabel('t [nbody]', fontsize=13)
plt.ylabel('|E0-E/E0| [nbody]', fontsize=13)
plt.title(integrator_list[3]+', e=0.99', fontsize=13)
plt.yscale("log")
plt.tight_layout()
plt.savefig(Eerror_each_plot[3], format="png",dpi=300)
plt.show()