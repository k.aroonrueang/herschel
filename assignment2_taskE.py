import numpy as np
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
import fireworks.ic as fic
import time
import matplotlib.pyplot as plt
import pickle
import matplotlib.ticker as ticker
plt.style.use('default')
import os

'''plz check all updates of Fireworks on https://gitlab.com/k.aroonrueang/herschel'''

### --- Create a directoriy to save the result
parent_dir = './plot_AS2E/'
if not os.path.exists(parent_dir): 
    os.makedirs(parent_dir)

### --- Select a grid of Number of particles (N)
N_arr = [3,10,50,1e2,5e2,1e3,2e3,3e3,5e3]
N_arr = [int(i) for i in N_arr] #just convert float to integer

### --- Define initial conditions
pos_min,pos_max = (0.,10.)
vel_min,vel_max = (0.,20.)
mass_min,mass_max = (1.,5.)

''' What I am doing here is to separate what I wanna do in different parts b/c it takes time for the run_time part 
and I am too lazy to wait for it again and again. '''
# set what you want to do: 0=dont execute it, 1=execute it
run_time = 1
save_file = 1
plot = 1

### --- Evaluate the time required to complete acceleration estimate
if run_time: #run time or not
    t_direct = []
    t_vector = []
    t_pyfalcon = []
    
    for N in N_arr:
        particles = fic.ic_random_uniform(N, pos_min, pos_max, vel_min, vel_max, mass_min, mass_max)
        
        # direct
        t1_direct = time.perf_counter()
        acc_direct = fdyn.acceleration_direct(particles)
        t2_direct = time.perf_counter()
        dt_direct = t2_direct-t1_direct
        t_direct.append(dt_direct)
        
        # vector
        t1_vector = time.perf_counter()
        acc_vector = fdyn.acceleration_direct_vectorised(particles)
        t2_vector = time.perf_counter()
        dt_vector = t2_vector-t1_vector
        t_vector.append(dt_vector)
        
        # pyfalcon
        t1_pyfalcon = time.perf_counter()
        acc_pyfalcon = fdyn.acceleration_pyfalcon(particles)
        t2_pyfalcon = time.perf_counter()
        dt_pyfalcon = t2_pyfalcon-t1_pyfalcon
        t_pyfalcon.append(dt_pyfalcon)

### --- save files
if save_file: #save file or not        
    pickle.dump(N_arr, open(parent_dir+'Nparticles.p', 'wb'))
    pickle.dump(t_direct, open(parent_dir+'t_direct.p', 'wb'))
    pickle.dump(t_vector, open(parent_dir+'t_vector.p', 'wb'))
    pickle.dump(t_pyfalcon, open(parent_dir+'t_pyfalcon.p', 'wb'))

### --- Make a plot Nparticles vs time
if plot: #plot new graph or not
    # open file
    N_arr = pickle.load(open(parent_dir+'Nparticles.p', 'rb'))
    t_direct = pickle.load(open(parent_dir+'t_direct.p', 'rb'))
    t_vector = pickle.load(open(parent_dir+'t_vector.p', 'rb'))
    t_pyfalcon = pickle.load(open(parent_dir+'t_pyfalcon.p', 'rb'))
        
    # plot
    fig, ax = plt.subplots(figsize=(8,5))
    fig.patch.set_facecolor('white')
    plt.plot(N_arr, t_direct, color='r',linestyle="-",label='Direct')
    plt.plot(N_arr, t_vector, color='b',linestyle="-",label='Vector')
    plt.plot(N_arr, t_pyfalcon, color='g',linestyle="-",label='Pyfalcon')
    
    # plot theoretical expectation lines
    N_arr = np.array(N_arr)
    plt.plot(N_arr,N_arr*(N_arr-1), color='r',linestyle="--",alpha=0.5,label='Direct&Vector-theory [O(N^2)]')
    plt.plot(N_arr,N_arr*np.log(N_arr), color='g',linestyle="--",alpha=0.5,label='Pyfalcon-theory [O(NlnN)]')
    
    plt.legend(fontsize='10', loc='upper left')
    plt.xlim(np.min(N_arr), np.max(N_arr))
    plt.xlabel('Nparticles', fontsize=12)
    plt.ylabel('Time [sec]', fontsize=13)
    plt.xticks(N_arr, fontsize=10)
    plt.xscale("log")
    plt.yscale("log")
    plt.axhline(y = 0.3 ,color = 'k', linestyle="-")
    plt.text(5.5e3, 0.25, 'Time=0.3 [sec]', color='k',fontsize=9)
    
    plt.tight_layout()
    plt.savefig(parent_dir+'AS2E_Npar-vs-time.png', format="png",dpi=300)   
    plt.show()