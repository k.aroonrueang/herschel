#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:37:58 2024

@author: mamodnoii
"""
import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
import fireworks.nbodylib.integrators as fint
import fireworks.nbodylib.timesteps as timesteps
import matplotlib.pyplot as plt
import pickle
import os

from importlib import reload
import Final_project_function as H
reload(H)

path_img_32='./Final_project_data/Images_for_videos/video_3-2/'
path_img_33='./Final_project_data/Images_for_videos/video_3-3/'

############## for 3-2 ##############
### Simulation
Nall=int(1e4)
pos_com=[40,20,0]; vel_com=[-0.2,0,0]
particles=fic.ic_2galaxies(Nall, pos_com, vel_com) #generate IC
ti=0.
tf=210.
nstep=int(1e3)
mass_arr,pos_dict,vel_dict=H.sim_galaxy(particles,tf,nstep) #simulation
tstep=tf/nstep
t_arr=np.arange(ti+tstep, tf+tstep, tstep)
t_display=np.linspace(ti, tf, num=120, endpoint=True)
idx_display=np.linspace(0, nstep, num=120, endpoint=True)
idx_display=idx_display.astype(int)
key_pos_display=['pos_'+str(idx) for idx in idx_display]
key_vel_display=['vel_'+str(idx) for idx in idx_display]
for i in range(len(idx_display)):
    t=t_display[i]
    key_pos=key_pos_display[i]
    key_vel=key_vel_display[i]
    R1,R2,vphi1,vphi2=H.compute_vphi_vs_R_simple_2galaxies(pos_dict, key_pos, vel_dict, key_vel)
    Rmid1,Rmid2,sigma1,sigma2=H.compute_surface_density_2galaxies(mass_arr, pos_dict, key_pos, vel_dict, key_vel, bins=20)
    
    pos_arr1 = pos_dict[key_pos][:Nall//2]
    vel_arr1 =vel_dict[key_vel][:Nall//2]
    pos_arr2 = pos_dict[key_pos][Nall//2:]
    vel_arr2 =vel_dict[key_vel][Nall//2:]
    x1=[pos_arr1[j][0] for j in range(pos_arr1.shape[0])]
    y1=[pos_arr1[j][1] for j in range(pos_arr1.shape[0])]
    x2=[pos_arr2[j][0] for j in range(pos_arr2.shape[0])]
    y2=[pos_arr2[j][1] for j in range(pos_arr2.shape[0])]

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(8, 2.5))
    fig.subplots_adjust(hspace =3, wspace=3)
    #fig.text(-25, 70,'t='+"{:.2e}".format(t)+' [Nbody]',fontsize=9)
    #Disc geometry
    ax[0].scatter(x1,y1,s=0.7,c='g',label='Galaxy1')
    ax[0].scatter(x1[0],y1[0],marker='*',s=20,c='b',zorder=10,label='GC1')
    ax[0].scatter(x2,y2,s=0.7,c='orange',label='Galaxy2')
    ax[0].scatter(x2[0],y2[0],marker='*',s=20,c='r',zorder=10,label='GC2')
    ax[0].set_title('t='+"{:.2e}".format(t)+' [Nbody], Disc geometry',fontsize=10)
    ax[0].set_xlim(-30, 65)
    ax[0].set_ylim(-30, 65)
    ax[0].tick_params(axis='both', which='major', labelsize=6)
    ax[0].set_xlabel('x [Nbody]',fontsize=7)
    ax[0].set_ylabel('y [Nbody]',fontsize=7)
    ax[0].legend(fontsize=5,ncols=2,loc='upper right')
    #Surface density profile'
    ax[1].plot(Rmid1,sigma1,linewidth=1.5,c='b',alpha=1.,zorder=-5,label='Galaxy1')
    ax[1].plot(Rmid2,sigma2,linewidth=1.5,c='r',alpha=0.8,zorder=5,label='Galaxy2')
    ax[1].set_title('Surface density profile',fontsize=10)
    ax[1].set_xlim(0,49)
    ax[1].set_ylim(8e-14,2e-9)
    ax[1].set_yscale('log')
    ax[1].tick_params(axis='both', which='major', labelsize=6)
    ax[1].set_ylabel('Surface number density [Nbody]',fontsize=7)
    ax[1].set_xlabel('Rcyl [Nbody]',fontsize=7)
    ax[1].legend(fontsize=5,loc='upper right')
    #Rotation curve
    ax[2].scatter(R1,vphi1,s=0.7,c='b',alpha=1.,zorder=-5,label='Galaxy1')
    ax[2].scatter(R2,vphi2,s=0.7,c='r',alpha=0.5,zorder=5,label='Galaxy2')
    ax[2].set_title('Rotation curve',fontsize=10)
    ax[2].set_xlim(-2,50)
    ax[2].set_ylim(-0.75,0.75)
    ax[2].tick_params(axis='both', which='major', labelsize=6)
    ax[2].set_ylabel('Vphi [Nbody]',fontsize=7)
    ax[2].set_xlabel('Rcyl [Nbody]',fontsize=7)
    ax[2].legend(fontsize=5,loc='upper right')
    ax[2].axhline(y=0,color='k',linestyle='--',zorder=-10,linewidth=1)
    
    plt.tight_layout()
    plt.savefig(path_img_32+str(i+1)+'.png', format="png",dpi=200)
    plt.show()

############## for 3-2 ##############
### Simulation
Nall=int(1e4)
pos_com=[40,20,0]; vel_com=[-0.2,0,0]
particles=fic.ic_2galaxies_counter_rotating(Nall, pos_com, vel_com) #generate IC
ti=0.
tf=210.
nstep=int(1e3)
mass_arr,pos_dict,vel_dict=H.sim_galaxy(particles,tf,nstep) #simulation
tstep=tf/nstep
t_arr=np.arange(ti+tstep, tf+tstep, tstep)
t_display=np.linspace(ti, tf, num=120, endpoint=True)
idx_display=np.linspace(0, nstep, num=120, endpoint=True)
idx_display=idx_display.astype(int)
key_pos_display=['pos_'+str(idx) for idx in idx_display]
key_vel_display=['vel_'+str(idx) for idx in idx_display]
for i in range(len(idx_display)):
    t=t_display[i]
    key_pos=key_pos_display[i]
    key_vel=key_vel_display[i]
    R1,R2,vphi1,vphi2=H.compute_vphi_vs_R_simple_2galaxies(pos_dict, key_pos, vel_dict, key_vel)
    Rmid1,Rmid2,sigma1,sigma2=H.compute_surface_density_2galaxies(mass_arr, pos_dict, key_pos, vel_dict, key_vel, bins=20)
    
    pos_arr1 = pos_dict[key_pos][:Nall//2]
    vel_arr1 =vel_dict[key_vel][:Nall//2]
    pos_arr2 = pos_dict[key_pos][Nall//2:]
    vel_arr2 =vel_dict[key_vel][Nall//2:]
    x1=[pos_arr1[j][0] for j in range(pos_arr1.shape[0])]
    y1=[pos_arr1[j][1] for j in range(pos_arr1.shape[0])]
    x2=[pos_arr2[j][0] for j in range(pos_arr2.shape[0])]
    y2=[pos_arr2[j][1] for j in range(pos_arr2.shape[0])]

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(8, 2.5))
    fig.subplots_adjust(hspace =3, wspace=3)
    
    #Disc geometry
    ax[0].scatter(x1,y1,s=0.7,c='g',label='Galaxy1')
    ax[0].scatter(x1[0],y1[0],marker='*',s=20,c='b',zorder=10,label='GC1')
    ax[0].scatter(x2,y2,s=0.7,c='orange',label='Galaxy2')
    ax[0].scatter(x2[0],y2[0],marker='*',s=20,c='r',zorder=10,label='GC2')
    ax[0].set_title('t='+"{:.2e}".format(t)+' [Nbody], Disc geometry',fontsize=10)
    ax[0].set_xlim(-30, 65)
    ax[0].set_ylim(-30, 65)
    ax[0].tick_params(axis='both', which='major', labelsize=6)
    ax[0].set_xlabel('x [Nbody]',fontsize=7)
    ax[0].set_ylabel('y [Nbody]',fontsize=7)
    ax[0].legend(fontsize=5,ncols=2,loc='upper right')
    #Surface density profile'
    ax[1].plot(Rmid1,sigma1,linewidth=1.5,c='b',alpha=1.,zorder=-5,label='Galaxy1')
    ax[1].plot(Rmid2,sigma2,linewidth=1.5,c='r',alpha=0.8,zorder=5,label='Galaxy2')
    ax[1].set_title('Surface density profile',fontsize=10)
    ax[1].set_xlim(0,49)
    ax[1].set_ylim(8e-14,2e-9)
    ax[1].set_yscale('log')
    ax[1].tick_params(axis='both', which='major', labelsize=6)
    ax[1].set_ylabel('Surface number density [Nbody]',fontsize=7)
    ax[1].set_xlabel('Rcyl [Nbody]',fontsize=7)
    ax[1].legend(fontsize=5,loc='upper right')
    #Rotation curve
    ax[2].scatter(R1,vphi1,s=0.7,c='b',alpha=1.,zorder=-5,label='Galaxy1')
    ax[2].scatter(R2,vphi2,s=0.7,c='r',alpha=0.5,zorder=5,label='Galaxy2')
    ax[2].set_title('Rotation curve',fontsize=10)
    ax[2].set_xlim(-2,50)
    ax[2].set_ylim(-0.75,0.75)
    ax[2].tick_params(axis='both', which='major', labelsize=6)
    ax[2].set_ylabel('Vphi [Nbody]',fontsize=7)
    ax[2].set_xlabel('Rcyl [Nbody]',fontsize=7)
    ax[2].legend(fontsize=5,loc='upper right')
    ax[2].axhline(y=0,color='k',linestyle='--',zorder=-10,linewidth=1)
    
    plt.tight_layout()
    plt.savefig(path_img_33+str(i+1)+'.png', format="png",dpi=200)
    plt.show()