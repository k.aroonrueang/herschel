#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 13:48:11 2024

@author: mamodnoii
"""

import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
import fireworks.nbodylib.integrators as fint
import fireworks.nbodylib.timesteps as timesteps
import matplotlib.pyplot as plt
import pickle

##### function for simulating galaxy evolution
def sim_galaxy(particles: Particles, tf: float, nstep: int):
    ti = 0.
    tstep=tf/nstep
    t_arr=np.arange(ti+tstep, tf+tstep, tstep)
    key_pos = ['pos_'+str(i+1) for i in range(len(t_arr))]
    key_vel = ['vel_'+str(i+1) for i in range(len(t_arr))]
    pos_dict={}; vel_dict={}
    pos_dict['pos_0'] = particles.pos
    vel_dict['vel_0'] = particles.vel
    for idx in range(len(t_arr)):
        particles, _, acc, jerk, potential=fint.integrator_leapfrog(particles=particles,
                            tstep=tstep,
                            acceleration_estimator=fdyn.acceleration_pyfalcon,
                            softening=5.,
                            external_accelerations = None)
        pos_dict[key_pos[idx]]= particles.pos
        vel_dict[key_vel[idx]]= particles.vel
    mass_arr = particles.mass
    return mass_arr,pos_dict,vel_dict

##### function for computing vphi (Cylindrical) from velocities and positions in Cartesian coord.
def vphi_compute(pos_arr: list, vel_arr: list):
    x=np.array([pos_arr[i][0] for i in range(len(pos_arr))])
    y=np.array([pos_arr[i][1] for i in range(len(pos_arr))])
    vx=np.array([vel_arr[i][0] for i in range(len(vel_arr))])
    vy=np.array([vel_arr[i][1] for i in range(len(vel_arr))])
    phi=np.arctan2(y,x)
    vphi = -np.sin(phi)*vx + np.cos(phi)*vy
    return x,y,vx,vy,phi,vphi

##### function for computing vR (Cylindrical) from velocities and positions in Cartesian coord.
def vR_compute(pos_arr: list, vel_arr: list):
    x=np.array([pos_arr[i][0] for i in range(len(pos_arr))])
    y=np.array([pos_arr[i][1] for i in range(len(pos_arr))])
    vx=np.array([vel_arr[i][0] for i in range(len(vel_arr))])
    vy=np.array([vel_arr[i][1] for i in range(len(vel_arr))])
    phi=np.arctan2(y,x)
    vR = np.cos(phi)*vx + np.sin(phi)*vy
    return x,y,vx,vy,phi,vR

##### function for rescaling/recentering all particles wrt the center one -> can convert x,y -> R
def rescale_system(pos_arr: list, vel_arr: list):
    pos_com=np.array(pos_arr[0])
    vel_com=np.array(vel_arr[0])
    pos_rescale=np.array(pos_arr)-pos_com
    vel_rescale=np.array(vel_arr)-vel_com
    return pos_rescale,vel_rescale

##### function for computing vphi vs R (simple) for 1 galaxy
def compute_vphi_vs_R_simple_1galaxy(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str):
    ### COMPUTATION
    pos_arr = pos_dict[pos_key]
    vel_arr = vel_dict[vel_key]
    pos_re_arr,vel_re_arr=rescale_system(pos_arr, vel_arr)
    x,y,vx,vy,phi,vphi=vphi_compute(pos_re_arr,vel_re_arr)
    R=np.sqrt(x**2+y**2)
    return R,vphi

##### function for computing vphi vs R (simple) for 2 galaxies
def compute_vphi_vs_R_simple_2galaxies(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vphi1=vphi_compute(pos_re_arr1,vel_re_arr1)
    R1=np.sqrt(x1**2+y1**2)
    
    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vphi2=vphi_compute(pos_re_arr2,vel_re_arr2)
    R2=np.sqrt(x2**2+y2**2)

    return R1,R2,vphi1,vphi2

##### function for computing vR vs R (simple) for 2 galaxies
def compute_vR_vs_R_simple_2galaxies(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vR1=vR_compute(pos_re_arr1,vel_re_arr1)
    R1=np.sqrt(x1**2+y1**2)
    
    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vR2=vR_compute(pos_re_arr2,vel_re_arr2)
    R2=np.sqrt(x2**2+y2**2)

    return R1,R2,vR1,vR2

##### function for computing vz vs R (simple) for 2 galaxies
def compute_vz_vs_R_simple_2galaxies(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vR1=vR_compute(pos_re_arr1,vel_re_arr1)
    vz1=[i[2] for i in vel_arr1]
    R1=np.sqrt(x1**2+y1**2)
    
    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vR2=vR_compute(pos_re_arr2,vel_re_arr2)
    vz2=[i[2] for i in vel_arr2]
    R2=np.sqrt(x2**2+y2**2)

    return R1,R2,vz1,vz2

##### function for plotting vphi vs R (simple) for 1 galaxy
def plot_vphi_simple_1galaxy(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, figsize=(8, 6)):
    ### COMPUTATION
    pos_arr = pos_dict[pos_key]
    vel_arr = vel_dict[vel_key]
    pos_re_arr,vel_re_arr=rescale_system(pos_arr,vel_arr)
    x,y,vx,vy,phi,vphi=vphi_compute(pos_re_arr,vel_re_arr)
    R=np.sqrt(x**2+y**2)
    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    ax.scatter(R,vphi,s=1,c='g')
    ax.set_xlabel('Rcyl [Nbody]')
    ax.set_ylabel('Vphi [Nbody]')
    plt.show()
    
    
##### function for plotting vphi vs R (simple) for 2 galaxies
def plot_vphi_simple_2galaxies(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, figsize=(8, 6)):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vphi1=vphi_compute(pos_re_arr1,vel_re_arr1)
    R1=np.sqrt(x1**2+y1**2)

    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vphi2=vphi_compute(pos_re_arr2,vel_re_arr2)
    R2=np.sqrt(x2**2+y2**2)

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=figsize)
    fig.text(0.5, 0.01, 'Rcyl [Nbody]', ha='center')
    fig.text(0.01, 0.5, 'Vphi [Nbody]', va='center', rotation='vertical')
    ax[0].scatter(R1,vphi1,s=1,c='g')
    ax[0].set_title('Galaxy 1')
    ax[0].set_xlim(-2,50)
    ax[0].set_ylim(-0.75,0.75)
    ax[1].scatter(R2,vphi2,s=1,c='b')
    ax[1].set_title('Galaxy 2')
    ax[1].set_xlim(-2,50)
    ax[1].set_ylim(-0.75,0.75)
    plt.show()
    
##### function for plotting vphi vs R (bin-smoother) for 1 galaxy
def plot_vphi_vs_R_bin_1galaxy(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, bins:int, figsize=(8, 6)):
    ### COMPUTATION
    pos_arr = pos_dict[pos_key]
    vel_arr =vel_dict[vel_key]
    pos_re_arr,vel_re_arr=rescale_system(pos_arr,vel_arr)
    x,y,vx,vy,phi,vphi=vphi_compute(pos_re_arr,vel_re_arr)
    R=np.sqrt(x**2+y**2)
    R_bin = np.histogram(R, bins=bins,density=None)
    R_num = R_bin[0]
    R_edge = R_bin[1]
    R_mid = []
    Vphi_avg = []
    for i in range(len(R_num)-1):
        mid = (R_edge[i]+R_edge[i+1])/2.
        selection= (R > R_edge[i]) & (R <= R_edge[i+1])
        Vphi_avg.append(np.average(vphi[selection]))
        R_mid.append(mid)

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    ax.scatter(R_mid,Vphi_avg,s=1,c='g')
    ax.set_xlabel('Rcyl [Nbody]')
    ax.set_ylabel('Vphi [Nbody]')
    plt.show()
    
##### function for plotting vphi vs R (bin-smoother) for 2 galaxies
def plot_vphi_vs_R_bin_2galaxies(pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, bins:int, figsize=(8, 6)):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vphi1=vphi_compute(pos_re_arr1,vel_re_arr1)
    R1=np.sqrt(x1**2+y1**2)
    R_bin1 = np.histogram(R1, bins=bins,density=None)
    R_num1 = R_bin1[0]
    R_edge1 = R_bin1[1]
    R_mid1 = []
    Vphi_avg1 = []
    for i in range(len(R_num1)-1):
        mid1 = (R_edge1[i]+R_edge1[i+1])/2.
        selection1 = (R1 > R_edge1[i]) & (R1 <= R_edge1[i+1])
        Vphi_avg1.append(np.average(vphi1[selection1]))
        R_mid1.append(mid1)
        
    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vphi2=vphi_compute(pos_re_arr2,vel_re_arr2)
    R2=np.sqrt(x2**2+y2**2)
    R_bin2 = np.histogram(R2, bins=bins,density=None)
    R_num2 = R_bin2[0]
    R_edge2 = R_bin2[1]
    R_mid2 = []
    Vphi_avg2 = []
    for i in range(len(R_num2)-1):
        mid2 = (R_edge2[i]+R_edge2[i+1])/2.
        selection2 = (R2 > R_edge2[i]) & (R2 <= R_edge2[i+1])
        Vphi_avg2.append(np.average(vphi2[selection2]))
        R_mid2.append(mid2)

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=figsize)
    fig.text(0.5, 0.04, 'Rcyl [Nbody]', ha='center')
    fig.text(0.04, 0.5, 'Vphi [Nbody]', va='center', rotation='vertical')
    ax[0].scatter(R_mid1,Vphi_avg1,s=1,c='g')
    ax[0].set_title('Galaxy 1')
    ax[1].scatter(R_mid2,Vphi_avg2,s=1,c='b')
    ax[1].set_title('Galaxy 2')
    plt.show()
    
##### function for computing a disc surface profile for 1 galaxy   
def compute_surface_density_1galaxy(mass_arr: list, pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, bins: int):
    ### COMPUTATION
    pos_arr = pos_dict[pos_key]
    vel_arr =vel_dict[vel_key]
    pos_re_arr,vel_re_arr=rescale_system(pos_arr,vel_arr)
    x,y,vx,vy,phi,vphi=vphi_compute(pos_re_arr,vel_re_arr)
    R=np.sqrt(x**2+y**2)
    R_bin=np.histogram(R, bins=bins,density=None)
    R_num = R_bin[0]
    R_edge = R_bin[1]
    Aring=[]
    Mring=[]
    R_mid=[]
    for i in range(len(R_num)-1):
        A = np.pi*(R_edge[i+1]**2-R_edge[i]**2)
        selection = (R > R_edge[i]) & (R <= R_edge[i+1])
        M = np.sum(mass_arr[selection])
        mid = (R_edge[i+1]+R_edge[i])/2.
        Aring.append(A)
        Mring.append(M)
        R_mid.append(mid)
    sigma=np.array(Mring)/np.array(Aring)
    return R_mid,sigma

##### function for computing disc surface profiles for 2 galaxies
def compute_surface_density_2galaxies(mass_arr: list, pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, bins: int):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vphi1=vphi_compute(pos_re_arr1,vel_re_arr1)
    R1=np.sqrt(x1**2+y1**2)
    mass_arr1=mass_arr[:Nall//2]
    R_bin1=np.histogram(R1, bins=bins,density=None)
    R_num1 = R_bin1[0]
    R_edge1 = R_bin1[1]
    Aring1=[]
    Mring1=[]
    R_mid1=[]
    for i in range(len(R_num1)-1):
        A1 = np.pi*(R_edge1[i+1]**2-R_edge1[i]**2)
        selection1 = (R1 > R_edge1[i]) & (R1 <= R_edge1[i+1])
        M1 = np.sum(mass_arr1[selection1])
        mid1 = (R_edge1[i+1]+R_edge1[i])/2.
        Aring1.append(A1)
        Mring1.append(M1)
        R_mid1.append(mid1)
    sigma1=np.array(Mring1)/np.array(Aring1)

    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vphi2=vphi_compute(pos_re_arr2,vel_re_arr2)
    R2=np.sqrt(x2**2+y2**2)
    mass_arr2=mass_arr[Nall//2:]
    R_bin2=np.histogram(R2, bins=bins,density=None)
    R_num2 = R_bin2[0]
    R_edge2 = R_bin2[1]
    Aring2=[]
    Mring2=[]
    R_mid2=[]
    for i in range(len(R_num2)-1):
        A2 = np.pi*(R_edge2[i+1]**2-R_edge2[i]**2)
        selection2 = (R2 > R_edge2[i]) & (R2 <= R_edge2[i+1])
        M2 = np.sum(mass_arr2[selection2])
        mid2 = (R_edge2[i+1]+R_edge2[i])/2.
        Aring2.append(A2)
        Mring2.append(M2)
        R_mid2.append(mid2)
    sigma2=np.array(Mring2)/np.array(Aring2)
    return R_mid1,R_mid2,sigma1,sigma2

##### function for plotting a disc surface profile for 1 galaxy
def plot_surface_density_1galaxy(mass_arr: list, pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, bins: int, figsize=(8, 6)):
    ### COMPUTATION
    pos_arr = pos_dict[pos_key]
    vel_arr =vel_dict[vel_key]
    pos_re_arr,vel_re_arr=rescale_system(pos_arr,vel_arr)
    x,y,vx,vy,phi,vphi=vphi_compute(pos_re_arr,vel_re_arr)
    R=np.sqrt(x**2+y**2)
    R_bin=np.histogram(R, bins=bins,density=None)
    R_num = R_bin[0]
    R_edge = R_bin[1]
    Aring=[]
    Mring=[]
    R_mid=[]
    for i in range(len(R_num)-1):
        A = np.pi*(R_edge[i+1]**2-R_edge[i]**2)
        selection = (R > R_edge[i]) & (R <= R_edge[i+1])
        M = np.sum(mass_arr[selection])
        mid = (R_edge[i+1]+R_edge[i])/2.
        Aring.append(A)
        Mring.append(M)
        R_mid.append(mid)
    sigma=np.array(Mring)/np.array(Aring)

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    fig.text(0.5, 0.04, 'Rcyl [Nbody]', ha='center')
    fig.text(0.04, 0.5, 'Surface number density [Nbody]', va='center', rotation='vertical')
    ax[0].scatter(R_mid,sigma,s=1,c='g')
    ax[0].set_yscale('log')
    plt.show()
    
##### function for plotting disc surface profiles for 2 galaxies
def plot_surface_density_2galaxies(mass_arr: list, pos_dict: list, pos_key: str, vel_dict: list, vel_key: str, bins: int, figsize=(8, 6)):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    vel_arr1 =vel_dict[vel_key][:Nall//2]
    pos_re_arr1,vel_re_arr1=rescale_system(pos_arr1,vel_arr1)
    x1,y1,vx1,vy1,phi1,vphi1=vphi_compute(pos_re_arr1,vel_re_arr1)
    R1=np.sqrt(x1**2+y1**2)
    mass_arr1=mass_arr[:Nall//2]
    R_bin1=np.histogram(R1, bins=bins,density=None)
    R_num1 = R_bin1[0]
    R_edge1 = R_bin1[1]
    Aring1=[]
    Mring1=[]
    R_mid1=[]
    for i in range(len(R_num1)-1):
        A1 = np.pi*(R_edge1[i+1]**2-R_edge1[i]**2)
        selection1 = (R1 > R_edge1[i]) & (R1 <= R_edge1[i+1])
        M1 = np.sum(mass_arr1[selection1])
        mid1 = (R_edge1[i+1]+R_edge1[i])/2.
        Aring1.append(A1)
        Mring1.append(M1)
        R_mid1.append(mid1)
    sigma1=np.array(Mring1)/np.array(Aring1)

    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    vel_arr2 =vel_dict[vel_key][Nall//2:]
    pos_re_arr2,vel_re_arr2=rescale_system(pos_arr2,vel_arr2)
    x2,y2,vx2,vy2,phi2,vphi2=vphi_compute(pos_re_arr2,vel_re_arr2)
    R2=np.sqrt(x2**2+y2**2)
    mass_arr2=mass_arr[Nall//2:]
    R_bin2=np.histogram(R2, bins=bins,density=None)
    R_num2 = R_bin2[0]
    R_edge2 = R_bin2[1]
    Aring2=[]
    Mring2=[]
    R_mid2=[]
    for i in range(len(R_num2)-1):
        A2 = np.pi*(R_edge2[i+1]**2-R_edge2[i]**2)
        selection2 = (R2 > R_edge2[i]) & (R2 <= R_edge2[i+1])
        M2 = np.sum(mass_arr2[selection2])
        mid2 = (R_edge2[i+1]+R_edge2[i])/2.
        Aring2.append(A2)
        Mring2.append(M2)
        R_mid2.append(mid2)
    sigma2=np.array(Mring2)/np.array(Aring2)

    ### PLOTTING
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=figsize)
    fig.text(0.5, 0.04, 'Rcyl [Nbody]', ha='center')
    fig.text(0.04, 0.5, 'Surface number density [Nbody]', va='center', rotation='vertical')
    ax[0].scatter(R_mid1,sigma1,s=1,c='g')
    ax[1].scatter(R_mid2,sigma2,s=1,c='b')
    ax[0].set_title('Galaxy 1')
    ax[0].set_yscale('log')
    ax[1].set_yscale('log')
    ax[1].set_title('Galaxy 2')
    plt.show()
    
##### function for plotting 3D interaction
def plot_3Dinteraction(pos_dict: list, pos_key: str, figsize=(15,6)):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    x1=[pos_arr1[i][0] for i in range(pos_arr1.shape[0])]
    y1=[pos_arr1[i][1] for i in range(pos_arr1.shape[0])]
    z1=[pos_arr1[i][2] for i in range(pos_arr1.shape[0])]
    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    x2=[pos_arr2[i][0] for i in range(pos_arr2.shape[0])]
    y2=[pos_arr2[i][1] for i in range(pos_arr2.shape[0])]
    z2=[pos_arr2[i][2] for i in range(pos_arr2.shape[0])]

    ### plotting
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x1,y1,z1,s=1,c='b',label='Galaxy 1')
    ax.scatter(x2,y2,z2,s=1,c='g',label='Galaxy 2')
    ax.set_xlabel("x [nobody]",fontsize=10)
    ax.set_ylabel("y [nbody]",fontsize=10)
    ax.set_zlabel("z [nbody]",fontsize=10)
    plt.legend()
    plt.tight_layout(rect=(0, 0, 1, 1))
    plt.show()
    
##### function for plotting 2D interaction (xy plane)
def plot_xy_2Dinteraction(pos_dict: list, pos_key: str, figsize=(6,4)):
    ### COMPUTATION
    Nall=pos_dict[pos_key].shape[0]
    #galaxy 1
    pos_arr1 = pos_dict[pos_key][:Nall//2]
    x1=[pos_arr1[i][0] for i in range(pos_arr1.shape[0])]
    y1=[pos_arr1[i][1] for i in range(pos_arr1.shape[0])]
    #galaxy 2
    pos_arr2 = pos_dict[pos_key][Nall//2:]
    x2=[pos_arr2[i][0] for i in range(pos_arr2.shape[0])]
    y2=[pos_arr2[i][1] for i in range(pos_arr2.shape[0])]

    ### plotting
    fig = plt.figure(figsize=figsize)
    plt.scatter(x1,y1,s=1,c='b',label='Galaxy 1')
    plt.scatter(x2,y2,s=1,c='g',label='Galaxy 1')
    plt.xlabel("x [nbody]")
    plt.ylabel("y [nbody]")
    plt.legend()
    #plt.tight_layout(rect=(0, 0, 1, 1))
    plt.show()