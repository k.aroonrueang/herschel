"""
====================================================================================================================
Collection of functions to estimate the Gravitational forces and accelerations (:mod:`fireworks.nbodylib.dynamics`)
====================================================================================================================

This module contains a collection of functions to estimate acceleration due to
gravitational  forces.

Each method implemented in this module should follow the input-output structure show for the
template function  :func:`~acceleration_estimate_template`:

Every function needs to have two input parameters:

    - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
    - softening, it is the gravitational softening. The parameters need to be included even
        if the function is not using it. Use a default value of 0.

The function needs to return a tuple containing three elements:

    - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
        this element is mandatory it cannot be 0.
    - jerk, time derivative of the acceleration, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx3 numpy array.
    - pot, gravitational potential at the position of each particle. it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx1 numpy array.


"""
from typing import Optional, Tuple
import numpy as np
import numpy.typing as npt
from ..particles import Particles

try:
    import pyfalcon
    pyfalcon_load=True
except:
    pyfalcon_load=False

def acceleration_estimate_template(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty functions that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    acc  = np.zeros(len(particles))
    jerk = None
    pot = None

    return (acc,jerk,pot)


def acceleration_pyfalcon(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the fast-multipole gravity Dehnen2002 solver (https://arxiv.org/pdf/astro-ph/0202512.pdf)
    as implementd in pyfalcon (https://github.com/GalacticDynamics-Oxford/pyfalcon)

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: a Nx1 numpy array containing the gravitational potential at each particle position
    """

    if not pyfalcon_load: return ImportError("Pyfalcon is not available")

    acc, pot = pyfalcon.gravity(particles.pos,particles.mass,softening,kernel=0)
    jerk = None

    return acc, jerk, pot


def acceleration_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This is a function for estimating the force directly without any algorithm to reduce the time to estimate the force
    
    There are two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function returns a tuple containing three elements:

        - acc, the acceleration estimated for each particle, in a Nx3 numpy array,
        - jerk, time derivative of the acceleration, it is an optional value, let's it as None for now.
        - pot, gravitational potential at the position of each particle. it is an optional value, let's it as None for now.
    
    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, set to None
        - pot, Nx1 numpy array storing the potential at each particle position, set to None
    """
    
    N = len(particles)
    acc = np.zeros([N,3])
    jerk = None
    pot = None
    
    for i in range(N):
        for j in range(N):
            if i != j:
                
                # Distance components between particles i and j in Cartesian coordinate
                dx = particles.pos[i][0]-particles.pos[j][0]
                dy = particles.pos[i][1]-particles.pos[j][1]
                dz = particles.pos[i][2]-particles.pos[j][2]
                
                # Distance modulus
                r = np.sqrt(dx**2 + dy**2 + dz**2)
                
                # Force components between particles i and j in Cartesian coordinate
                acc[i][0] -= particles.mass[j]*dx/r**3 #Fx
                acc[i][1] -= particles.mass[j]*dy/r**3 #Fy
                acc[i][2] -= particles.mass[j]*dz/r**3 #Fz

    return (acc,jerk,pot)


def acceleration_direct_vectorised(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This is a function to estimate force by exploting the vecterising/matrix operations in Python 
    in order to reduce number of operations -> reduce the time to estimate the force.
    
    There are two input parameters:

       - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
       - softening, it is the gravitational softening. The parameters need to be included even
         if the function is not using it. Use a default value of 0.

    The function returns a tuple containing three elements:

       - acc, the acceleration estimated for each particle, in a Nx3 numpy array,
       - jerk, time derivative of the acceleration, it is an optional value, let's it as None for now.
       - pot, gravitational potential at the position of each particle. it is an optional value, let's it as None for now.
   
   :param particles: An instance of the class Particles
   :param softening: Softening parameter
   :return: A tuple with 3 elements:
 
       - acc, Nx3 numpy array storing the acceleration for each particle
       - jerk, Nx3 numpy array storing the time derivative of the acceleration, set to None
       - pot, Nx1 numpy array storing the potential at each particle position, set to None
   """

    N = len(particles)
    acc = np.zeros([N,3])
    jerk = None
    pot = None
    
    ## Distance components between particles i and j in Cartesian coordinate 
    # convert 2 [Nx1] arrays to 1 [NxN] array for each basis in Cartesian coordinate
    dx = (particles.pos[:,0]).reshape(N,1).T-(particles.pos[:,0]).reshape(N,1)
    dy = (particles.pos[:,1]).reshape(N,1).T-(particles.pos[:,1]).reshape(N,1)
    dz = (particles.pos[:,2]).reshape(N,1).T-(particles.pos[:,2]).reshape(N,1)
    # replace diagonal elements with int(0) to avoid round-off errors
    np.fill_diagonal(dx,0)
    np.fill_diagonal(dy,0)
    np.fill_diagonal(dz,0)
    
    ## Distance modulus
    r = np.sqrt(dx**2 + dy**2 + dz**2)
    # replace diagonal elements with float(1.) to avoid divided by 0 = inf (actually it is just a warning but so annoying)
    np.fill_diagonal(r,1.)
    
    ## calculate acc for each basis in Cartesian coordinate
    accx = -particles.mass.T@(dx/r**3)
    accy = -particles.mass.T@(dy/r**3)
    accz = -particles.mass.T@(dz/r**3)
    
    ## store acc in [Nx3] array
    acc[:,0] = accx
    acc[:,1] = accy
    acc[:,2] = accz
    
    return (acc,jerk,pot)


def acceleration_jerk_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This is a function to estimate jerk by exploting the vecterising/matrix operations in Python 
    in order to reduce number of operations -> reduce the time to estimate the force.
    
    There are two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """
    N = len(particles)
    acc = np.zeros([N,3])
    jerk = np.zeros([N,3])
    pot = None
    
    ## ri-rj for each basis (x,y,z) -> 2 Nx1 arrays -> NxN array for each basis
    dx = (particles.pos[:,0]).reshape(N,1).T-(particles.pos[:,0]).reshape(N,1)
    dy = (particles.pos[:,1]).reshape(N,1).T-(particles.pos[:,1]).reshape(N,1)
    dz = (particles.pos[:,2]).reshape(N,1).T-(particles.pos[:,2]).reshape(N,1)  
    #replace diagonal elements with 0 to avoid round-off errors
    np.fill_diagonal(dx,0)
    np.fill_diagonal(dy,0)
    np.fill_diagonal(dz,0)
    ## vi-vj for each basis (x,y,z) -> 2 Nx1 arrays -> NxN array for each basis
    dvx = (particles.vel[:,0]).reshape(N,1).T-(particles.vel[:,0]).reshape(N,1)
    dvy = (particles.vel[:,1]).reshape(N,1).T-(particles.vel[:,1]).reshape(N,1)
    dvz = (particles.vel[:,2]).reshape(N,1).T-(particles.vel[:,2]).reshape(N,1)  
    #replace diagonal elements with 0 to avoid round-off errors
    np.fill_diagonal(dvx,0)
    np.fill_diagonal(dvy,0)
    np.fill_diagonal(dvz,0)
    
    ## |ri_rj|
    r = np.sqrt(dx**2 + dy**2 + dz**2)
    #replace diagonal elements with 1. to avoid divided by 0 = inf
    np.fill_diagonal(r,1.)
    
    ## calculate acc for each basis
    accx = -particles.mass.T@(dx/r**3)
    accy = -particles.mass.T@(dy/r**3)
    accz = -particles.mass.T@(dz/r**3)
    # store acc in Nx3 array
    acc[:,0] = accx
    acc[:,1] = accy
    acc[:,2] = accz
    
    ## calculate jerk for each basis
    jerkx = -particles.mass.T@(dvx/r**3-3*(dx*dvx)*dx/r**5)
    jerky = -particles.mass.T@(dvy/r**3-3*(dy*dvy)*dy/r**5)
    jerkz = -particles.mass.T@(dvz/r**3-3*(dz*dvz)*dz/r**5)
    # store jerk in Nx3 array
    jerk[:,0] = jerkx
    jerk[:,1] = jerky
    jerk[:,2] = jerkz

       
    return (acc,jerk,pot)
