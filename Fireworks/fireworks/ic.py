"""
==============================================================
Initial conditions utilities , (:mod:`fireworks.ic`)
==============================================================

This module contains functions and utilities to generate
initial conditions for the Nbody simulations.
The basic idea is that each function/class should returns
an instance of the class :class:`~fireworks.particles.Particles`

"""

import numpy as np
from .particles import Particles

def ic_random_normal(N: int, mass: float=1) -> Particles:
    """
    Generate random initial condition drawing from a normal distribution
    (centred in 0 and with dispersion 1) for the position and velocity.
    The mass is instead the same for all the particles.

    :param N: number of particles to generate
    :param mass: mass of the particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    pos  = np.random.normal(size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    vel  = np.random.normal(size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    mass = np.ones(N)*mass

    return Particles(position=pos, velocity=vel, mass=mass)


def ic_random_uniform(N: int, pos_min: float, pos_max: float, vel_min: float, vel_max: float, mass_min: float, mass_max: float ) -> Particles:
    """
    Generate random initial condition drawing from a uniform distribution (same probability for all outcomes)
    within the minimum and maximum boundaries for positions, velocities, and masses that user can select

    :param N: number of particles to generate
    :param pos_min: minimum boundary of position of the particles
    :param pos_max: maximum boundary of position of the particles
    :param vel_min: minimum boundary of velocity of the particles
    :param vel_max: maximum boundary of velocity of the particles
    :param mass_min: minimum boundary of mass of the particles
    :param mass_max: maximum boundary of mass of the particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """
    
    pos  = np.random.uniform(pos_min,pos_max,size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    vel  = np.random.uniform(vel_min,vel_max,size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    mass = np.random.uniform(mass_min,mass_max,size=N) # Generate N 1D array

    return Particles(position=pos, velocity=vel, mass=mass)


def ic_two_body(mass1: float, mass2: float, rp: float, e: float):
    """
    Create initial conditions for a two-body system.
    By default the two bodies will placed along the x-axis at the
    closest distance rp.
    Depending on the input eccentricity the two bodies can be in a
    circular (e<1), parabolic (e=1) or hyperbolic orbit (e>1).

    :param mass1:  mass of the first body [nbody units]
    :param mass2:  mass of the second body [nbody units]
    :param rp: closest orbital distance [nbody units]
    :param e: eccentricity
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    Mtot=mass1+mass2

    if e==1.:
        vrel=np.sqrt(2*Mtot/rp)
    else:
        a=rp/(1-e)
        vrel=np.sqrt(Mtot*(2./rp-1./a))

    # To take the component velocities
    # V1 = Vcom - m2/M Vrel
    # V2 = Vcom + m1/M Vrel
    # we assume Vcom=0.
    v1 = -mass2/Mtot * vrel
    v2 = mass1/Mtot * vrel

    pos  = np.array([[0.,0.,0.],[rp,0.,0.]])
    vel  = np.array([[0.,v1,0.],[0.,v2,0.]])
    mass = np.array([mass1,mass2])

    return Particles(position=pos, velocity=vel, mass=mass)
    
def ic_1galaxy(N: int) -> Particles:
    """
    Create initial conditions for a disc galaxy with N stellar tracers by reading in data from Nbody_disc.csv

    :param N:  number of stellar tracers in the system
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    mass,x,y,z,vx,vy,vz=np.genfromtxt("Nbody_disc.csv",unpack=True,delimiter=',',skip_header=1,max_rows=N)
    pos=[]; vel=[]
    for i in range(len(mass)):
        posi=[x[i],y[i],z[i]]
        veli=[vx[i],vy[i],vz[i]]
        
        pos.append(posi)
        vel.append(veli)
        
    pos  = np.array(pos)
    vel  = np.array(vel)
    mass = np.array(mass)

    return Particles(position=pos, velocity=vel, mass=mass)

def ic_2galaxies(Nall: int, pos_com: list, vel_com: list) -> Particles:
    """
    Create initial conditions for two disc galaxies with N//2 stellar tracers for each galaxy
    by reading in data from Nbody_disc.csv
    
    After that, the position and velocity of galaxy2 will be rescaled by param pos_com and vel_com, respectively

    :param Nall: number of total stellar tracers in both galaxies (Nall//)2 for each galaxy)
    :param pos_com: position of the central mass which will be used to rescale the galaxy2 particles
    :param vel_com: velocity of the central mass which will be used to rescale the galaxy2 particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    with ordor of 1st galaxy -> 2nd galaxy in array
    """
    
    N=Nall//2
    
    #1st galaxy
    mass,x,y,z,vx,vy,vz=np.genfromtxt("Nbody_disc.csv",unpack=True,delimiter=',',skip_header=1,max_rows=N)
    pos=[]; vel=[]
    for i in range(len(mass)):
        posi=[x[i],y[i],z[i]]
        veli=[vx[i],vy[i],vz[i]]
        
        pos.append(posi)
        vel.append(veli)
    
    #2nd galaxy
    pos2=np.copy(pos); vel2=np.copy(vel)
    pos2+=pos_com; vel2+=vel_com
    for i in range(len(mass)):
        posi=[pos2[i][0],pos2[i][1],pos2[i][2]]
        veli=[vel2[i][0],vel2[i][1],vel2[i][2]]
        
        pos.append(posi)
        vel.append(veli)
        
    pos  = np.array(pos)
    vel  = np.array(vel)
    mass = np.array(list(mass)+list(mass))

    return Particles(position=pos, velocity=vel, mass=mass)


def ic_2galaxies_counter_rotating(Nall: int, pos_com: list, vel_com: list) -> Particles:
    """
    Create initial conditions for two disc galaxies with N//2 stellar tracers for each galaxy
    by reading in data from Nbody_disc.csv
    
    After that, the velocity of galaxy2 particles are multiplied by -1 to make it counter-rotate wrt the galaxy1 particles
       
    Finally, the position and velocity of galaxy2 will be rescaled by param pos_com and vel_com, respectively

    :param Nall: number of total stellar tracers in both galaxies (Nall//)2 for each galaxy)
    :param pos_com: position of the central mass which will be used to rescale the galaxy2 particles
    :param vel_com: velocity of the central mass which will be used to rescale the galaxy2 particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    with ordor of 1st galaxy -> 2nd galaxy in array
    """
    
    N=Nall//2
    
    #1st galaxy
    mass,x,y,z,vx,vy,vz=np.genfromtxt("Nbody_disc.csv",unpack=True,delimiter=',',skip_header=1,max_rows=N)
    pos=[]; vel=[]
    for i in range(len(mass)):
        posi=[x[i],y[i],z[i]]
        veli=[vx[i],vy[i],vz[i]]
        
        pos.append(posi)
        vel.append(veli)
    
    #2nd galaxy
    pos2=np.copy(pos); vel2=np.copy(vel)
    vel2*=-1
    pos2+=pos_com; vel2+=vel_com
    for i in range(len(mass)):
        posi=[pos2[i][0],pos2[i][1],pos2[i][2]]
        veli=[vel2[i][0],vel2[i][1],vel2[i][2]]
        
        pos.append(posi)
        vel.append(veli)
        
    pos  = np.array(pos)
    vel  = np.array(vel)
    mass = np.array(list(mass)+list(mass))

    return Particles(position=pos, velocity=vel, mass=mass)

def ic_2galaxies_inclination(Nall: int, pos_com: list, vel_com: list, inc_deg: float) -> Particles:
    """
    bah bah bah - dont use anymore
    """

    inc_rad=inc_deg*np.pi/180.
    N=Nall//2
    
    #1st galaxy
    mass,x,y,z,vx,vy,vz=np.genfromtxt("Nbody_disc.csv",unpack=True,delimiter=',',skip_header=1,max_rows=N)
    pos=[]; vel=[]
    for i in range(len(mass)):
        posi=[x[i],y[i],z[i]]
        veli=[vx[i],vy[i],vz[i]]
        
        pos.append(posi)
        vel.append(veli)
    
    #2nd galaxy
    pos2=np.copy(pos); vel2=np.copy(vel)
    pos2+=pos_com; vel2+=vel_com
    for i in range(len(mass)):
        pos2_x=np.cos(inc_rad)*pos2[i][0]-np.sin(inc_rad)*pos2[i][2]
        vel2_x=np.cos(inc_rad)*vel2[i][0]-np.sin(inc_rad)*vel2[i][2]
        pos2_z=np.sin(inc_rad)*pos2[i][0]+np.cos(inc_rad)*pos2[i][2]
        vel2_z=np.sin(inc_rad)*vel2[i][0]+np.cos(inc_rad)*vel2[i][2]
        
        posi=[pos2_x,pos2[i][1],pos2_z]
        veli=[vel2_x,vel2[i][1],vel2_z]
        
        pos.append(posi)
        vel.append(veli)
        
    pos  = np.array(pos)
    vel  = np.array(vel)
    mass = np.array(list(mass)+list(mass))

    return Particles(position=pos, velocity=vel, mass=mass)
