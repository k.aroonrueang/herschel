#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 14:37:58 2024

@author: mamodnoii
"""

import moviepy.video.io.ImageSequenceClip

path_img_32='./Images_for_videos/video_3-2/'
path_img_33='./Images_for_videos/video_3-3/'

fps=10

#for 3-2
img_list32=[path_img_32+str(i+1)+'.png' for i in range(120)]
clip32 = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(img_list32, fps=fps)
clip32.write_videofile('3-2_video.mp4')
#for 3-3
img_list33=[path_img_33+str(i+1)+'.png' for i in range(120)]
clip33 = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(img_list33, fps=fps)
clip33.write_videofile('3-3_video.mp4')