#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 17:49:08 2023

@author: mamodnoii
"""

import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
import fireworks.nbodylib.integrators as fint
import matplotlib.pyplot as plt
import fireworks.nbodylib.timesteps as timesteps
plt.style.use('default')
import os 

'''plz check all updates of Fireworks on https://gitlab.com/k.aroonrueang/herschel'''

###############################################################################
############ ------------ PRELIMINARY  ------------ ############

### --- Create directories and subdirectories to save all results
parent_dir = './plot_AS3D_adaptive-tstep_ecc0-99/'
if not os.path.exists(parent_dir): 
    os.makedirs(parent_dir)


### --- Initial Condition 
# -- Nbodyunit-values
#Mscale=10Msun
#m1_physical=10Msun
#m2_physical=20Msun
#Lscale=20AU
#rp_physical=2AU
m1_Nbody=1.
m2_Nbody=2.
rp_Nbody=0.1
e=0.99
particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)

#Calculate Tperiod
a_Nbody=-(m1_Nbody*m2_Nbody)/(2.*particles.Etot(softening= 0.0)[0])
#a_Nbody=rp_Nbody/(1.-e)
Tperiod = 2.*np.pi*np.sqrt(a_Nbody**3/(m1_Nbody+m2_Nbody))
tf = 10.*Tperiod

### --- name of plots
xy_Euler_plot = './plot_AS3D_adaptive-tstep_ecc0-99/AS3D_xy_Euler_ecc0-99_adaptive-tstep.png'
xy_LF_plot = './plot_AS3D_adaptive-tstep_ecc0-99/AS3D_xy_LF_ecc0-99_adaptive-tstep.png' 
xy_RK4_plot = './plot_AS3D_adaptive-tstep_ecc0-99/AS3D_xy_RK4_ecc0-99_adaptive-tstep.png'
xy_Tsunami_plot = './plot_AS3D_adaptive-tstep_ecc0-99/AS3D_xy_Tsunami_ecc0-99_adaptive-tstep.png'

Etot_path = './plot_AS3D_adaptive-tstep_ecc0-99/AS3D_Etot_'
Etot_plot = Etot_path+'ecc0-99_adaptive-tstep.png'
Eerror_plot = './plot_AS3D_adaptive-tstep_ecc0-99/AS3D_Eerror_ecc0-99_adaptive-tstep.png'
Eerror_path = './plot_AS3D_adaptive-tstep_ecc0-99/'
Eerror_each_plot = Eerror_path+'AS3D_Eerror_ecc0-99_adaptive-tstep.png'


###############################################################################
############ ------------ CALCULATION + XYPLOT  ------------ ############
### -----  fint.integrator_euler ------
particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)
t = 0.
x1_arr=[]
y1_arr=[]
x2_arr=[]
y2_arr=[]
t_Euler_arr=[]
Etot_Euler_arr=[]

tstep=1e-3 #give inital value otherwise get 0 tstep
t+=tstep
while(t<tf):
    t_Euler_arr.append(t)
    particles, _, acc, jerk, potential=fint.integrator_euler(particles=particles,
                        tstep=tstep,
                        acceleration_estimator=fdyn.acceleration_direct_vectorised,
                        softening=0.,
                        external_accelerations = None)
    tstep = timesteps.my_adaptive_timestep(particles) 
    t+=tstep
    
    x1_arr.append(particles.pos[0][0]) #m1 xassignment3_taskD_adaptive-tstep_e0-5.py
    y1_arr.append(particles.pos[0][1]) #m1 y
    x2_arr.append(particles.pos[1][0]) #m2 x
    y2_arr.append(particles.pos[1][1]) #m2 y
    Etot_Euler_arr.append(particles.Etot(softening= 0.0)[0])


x1_arr=np.array(x1_arr)
y1_arr=np.array(y1_arr)
x2_arr=np.array(x2_arr)
y2_arr=np.array(y2_arr)
t_Euler_arr=np.array(t_Euler_arr)
Etot_Euler_arr=np.array(Etot_Euler_arr)
  
### PLOT XY Euler
fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(x1_arr, y1_arr, color='r',linestyle="-",label="m1_Nbody=1")
plt.plot(x2_arr, y2_arr, color='b',linestyle="-",label="m2_Nbody=2")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('x [nbody]', fontsize=13)
plt.ylabel('y [nbody]', fontsize=13)
plt.title('Euler - Adaptive timestep, e=0.99', fontsize=13)
plt.tight_layout()
plt.savefig(xy_Euler_plot, format="png",dpi=300)
plt.show()


### -----  fint.integrator_leapfrog ------
particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)
t = 0.
x1_arr=[]
y1_arr=[]
x2_arr=[]
y2_arr=[]
t_LF_arr=[]
Etot_LF_arr=[]

tstep=1e-3 #give inital value otherwise get 0 tstep
t+=tstep
while(t<tf):
    t_LF_arr.append(t)
    particles, _, acc, jerk, potential=fint.integrator_leapfrog(particles=particles,
                        tstep=tstep,
                        acceleration_estimator=fdyn.acceleration_direct_vectorised,
                        softening=0.,
                        external_accelerations = None)
    tstep = timesteps.my_adaptive_timestep(particles) 
    t+=tstep
    
    x1_arr.append(particles.pos[0][0]) #m1 x
    y1_arr.append(particles.pos[0][1]) #m1 y
    x2_arr.append(particles.pos[1][0]) #m2 x
    y2_arr.append(particles.pos[1][1]) #m2 y
    Etot_LF_arr.append(particles.Etot(softening= 0.0)[0])


x1_arr=np.array(x1_arr)
y1_arr=np.array(y1_arr)
x2_arr=np.array(x2_arr)
y2_arr=np.array(y2_arr)
t_LF_arr=np.array(t_LF_arr)
Etot_LF_arr=np.array(Etot_LF_arr)
  
### PLOT XY LF
fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(x1_arr, y1_arr, color='r',linestyle="-",label="m1_Nbody=1")
plt.plot(x2_arr, y2_arr, color='b',linestyle="-",label="m2_Nbody=2")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('x [nbody]', fontsize=13)
plt.ylabel('y [nbody]', fontsize=13)
plt.title('Leapfrog - Adaptive timestep, e=0.99', fontsize=13)
plt.tight_layout()
plt.savefig(xy_LF_plot, format="png",dpi=300)
plt.show()


### -----  fint.integrator_rk4 ------
particles = fic.ic_two_body(mass1=m1_Nbody, mass2=m2_Nbody, rp=rp_Nbody, e=e)
t = 0.
x1_arr=[]
y1_arr=[]
x2_arr=[]
y2_arr=[]
t_RK4_arr=[]
Etot_RK4_arr=[]

tstep=1e-3 #give inital value otherwise get 0 tstep
t+=tstep
while(t<tf):
    t_RK4_arr.append(t)
    particles, _, acc, jerk, potential=fint.integrator_rk4(particles=particles,
                        tstep=tstep,
                        acceleration_estimator=fdyn.acceleration_direct_vectorised,
                        softening=0.,
                        external_accelerations = None)
    tstep = timesteps.my_adaptive_timestep(particles) 
    t+=tstep
    
    x1_arr.append(particles.pos[0][0]) #m1 x
    y1_arr.append(particles.pos[0][1]) #m1 y
    x2_arr.append(particles.pos[1][0]) #m2 x
    y2_arr.append(particles.pos[1][1]) #m2 y
    Etot_RK4_arr.append(particles.Etot(softening= 0.0)[0])


x1_arr=np.array(x1_arr)
y1_arr=np.array(y1_arr)
x2_arr=np.array(x2_arr)
y2_arr=np.array(y2_arr)
t_RK4_arr=np.array(t_RK4_arr)
Etot_RK4_arr=np.array(Etot_RK4_arr)

### PLOT XY RK4
fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(x1_arr, y1_arr, color='r',linestyle="-",label="m1_Nbody=1")
plt.plot(x2_arr, y2_arr, color='b',linestyle="-",label="m2_Nbody=2")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('x [nbody]', fontsize=13)
plt.ylabel('y [nbody]', fontsize=13)
plt.title('RK4 - Adaptive timestep, e=0.99', fontsize=13)
plt.tight_layout()
plt.savefig(xy_RK4_plot, format="png",dpi=300)
plt.show()


### -----  fint.integrator_tsunami ------
tcurrent = 0
x1_arr=[]
y1_arr=[]
x2_arr=[]
y2_arr=[]
step=0.01
t_Tsunami_arr=np.arange(tcurrent, tf+step, step)
Etot_Tsunami_arr=[]
efftime_arr=[]
tplot_Tsunami_arr=[]
for t in t_Tsunami_arr:
    tstep=t-tcurrent
    # skip this loop for tstep <= 0 (negative and zero tstep will raise error: you try to evolve the system that was already evolved)
    if tstep <= 0: continue 
    tplot_Tsunami_arr.append(tcurrent)
    particles, efftime,_, _,_=fint.integrator_tsunami(particles,tstep)
    tcurrent=tcurrent+efftime
    
    x1_arr.append(particles.pos[0][0]) #m1 x
    y1_arr.append(particles.pos[0][1]) #m1 y
    x2_arr.append(particles.pos[1][0]) #m2 x
    y2_arr.append(particles.pos[1][1]) #m2 y
    Etot_Tsunami_arr.append(particles.Etot(softening= 0.0)[0])
    efftime_arr.append(efftime)
  
x1_arr=np.array(x1_arr)
y1_arr=np.array(y1_arr)
x2_arr=np.array(x2_arr)
y2_arr=np.array(y2_arr)
Etot_Tsunami_arr=np.array(Etot_Tsunami_arr)
tplot_Tsunami_arr=np.array(tplot_Tsunami_arr)

### PLOT XY Tsunami
fig, ax = plt.subplots(1,figsize=(5,5)) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.scatter(x1_arr, y1_arr, marker='.', s=7,color='r',linestyle="None",label="m1_Nbody=1")
plt.scatter(x2_arr, y2_arr, marker='.', s=7, color='b',linestyle="None",label="m2_Nbody=2")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('x [nbody]', fontsize=13)
plt.ylabel('y [nbody]', fontsize=13)
plt.title('Tsunami - Adaptive timestep, e=0.99', fontsize=13)  
plt.tight_layout()
plt.savefig(xy_Tsunami_plot, format="png",dpi=300)
plt.show()


###############################################################################
############ ------------ ETOT_VS_TIME-PLOT for all ------------ ############

fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(t_Euler_arr,Etot_Euler_arr, color='C1',linestyle="-",label="Euler")
plt.plot(t_LF_arr,Etot_LF_arr, color='C2',linestyle="-",label="LF")
plt.plot(t_RK4_arr,Etot_RK4_arr, color='C3',linestyle="-",label="RK4")
plt.plot(tplot_Tsunami_arr,Etot_Tsunami_arr, color='C4',linestyle="-",label="Tsunami")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('t [nbody]', fontsize=13)
plt.ylabel('Etot [nbody]', fontsize=13)
plt.title('Adaptive timestep, e=0.99', fontsize=13)  
#plt.title('h = '+str(h), fontsize=15)
plt.tight_layout()
plt.savefig(Etot_plot, format="png",dpi=300)
plt.show()


###############################################################################
############ ------------ COMPUTE Eerror for all ------------ ############

### --- compute and store Eerror of Euler
E0=Etot_Euler_arr[0]
Eerror_Euler_arr = [np.abs((E-E0)/E0) for E in Etot_Euler_arr]
Eerror_Euler_avg = np.average(Eerror_Euler_arr)

### --- compute and store Eerror of LF
E0=Etot_LF_arr[0]
Eerror_LF_arr = [np.abs((E-E0)/E0) for E in Etot_LF_arr]
Eerror_LF_avg = np.average(Eerror_LF_arr)
    
### --- compute and store Eerror of RK4
E0=Etot_RK4_arr[0]
Eerror_RK4_arr = [np.abs((E-E0)/E0) for E in Etot_RK4_arr]
Eerror_RK4_avg = np.average(Eerror_RK4_arr)
    
### --- compute and store Eerror of Tsunami
E0=Etot_Tsunami_arr[0]
Eerror_Tsunami_arr = [np.abs((E-E0)/E0) for E in Etot_Tsunami_arr]
Eerror_Tsunami_avg = np.average(Eerror_Tsunami_arr)


###############################################################################
############ ------------ Eerror_VS_TIME-PLOT for all ------------ ############

fig, ax = plt.subplots(1) #plt.subplots(figsize=(7,6))
fig.patch.set_facecolor('white')
plt.plot(t_Euler_arr,Eerror_Euler_arr, color='C1',linestyle="-",label="Euler")
plt.plot(t_LF_arr,Eerror_LF_arr, color='C2',linestyle="-",label="LF")
plt.plot(t_RK4_arr,Eerror_RK4_arr, color='C3',linestyle="-",label="RK4")
plt.plot(tplot_Tsunami_arr,Eerror_Tsunami_arr, color='C4',linestyle="-",label="Tsunami")
plt.legend(fontsize='10', loc='upper right')
plt.xlabel('t [nbody]', fontsize=13)
plt.ylabel('|E0-E/E0| [nbody]', fontsize=13)
plt.title('Adaptive timestep, e=0.99', fontsize=13) 
#plt.title('h = '+str(h), fontsize=15)
plt.tight_layout()
plt.savefig(Eerror_each_plot, format="png",dpi=300)
plt.show()